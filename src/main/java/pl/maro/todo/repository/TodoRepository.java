package pl.maro.todo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.maro.todo.model.Todo;

@Repository
public interface TodoRepository extends JpaRepository<Todo, Long> {
}
