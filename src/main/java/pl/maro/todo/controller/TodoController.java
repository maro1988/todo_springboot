package pl.maro.todo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.maro.todo.model.Todo;
import pl.maro.todo.repository.TodoRepository;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin("*")
public class TodoController {

    @Autowired
    TodoRepository todoRepository;

    @GetMapping("todo_items")
    @CrossOrigin(origins = "http://localhost:8080")
    public List<Todo> getAll(){
        return todoRepository.findAll();
    }

    @GetMapping("todo_items/{id}")
    public ResponseEntity<Todo> getById(@PathVariable long id){
        return todoRepository.findById(id)
                .map(todo -> ResponseEntity.ok().body(todo))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("todo_items")
    public Todo ceateTodo(@Valid @RequestBody Todo todo){
        return todoRepository.save(todo);
    }

    @PutMapping("todo_items/{id}")
    public ResponseEntity<Todo> update(@PathVariable long id, @Valid @RequestBody Todo inputTodo){
        return todoRepository.findById(id).map(todo -> {
            todo.setContent(inputTodo.getContent());
            todo.setDone(inputTodo.isDone());
            todo.setUpdatedAt(inputTodo.getUpdatedAt());
            Todo updated = todoRepository.save(todo);
            return ResponseEntity.ok().body(todo);
        }).orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping("todo_items/{id}")
    public ResponseEntity<?> delete(@PathVariable long id){
        return todoRepository.findById(id).map(todo -> {
            todoRepository.delete(todo);
            return ResponseEntity.ok().build();
        }).orElse(ResponseEntity.notFound().build());
    }
}
